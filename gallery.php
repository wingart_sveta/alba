<!DOCTYPE html>
<html lang="en">
<head>
    <title>Gallery</title>
    <meta name = "format-detection" content = "telephone=no" />
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/html5.js"></script>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
       
    <![endif]-->

    <!--[if lt IE 9]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
         <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <![endif]-->
</head>
<body>
    <!--==============================content===========================-->
    <div class="content_top2 al_center">
        <div class="main">
        <!--==============================header===========================-->
            <?php include("main_blocks/header.php") ?>
            <h1 class="title6 black metaM">Галерея участников</h1>
            <span class="d_bl pragmaticaLight font_18 lh_30 gallery_title_small pt10 mb30">Участвуйте в фотоконкурсе и получите шанс выиграть поездку в Италию <br> и множество других ценных <a href="#" class="lk1 pragmaticaMedium font_18 lh_30"> призов!</a></span>
        

            <div class="pb27">
                <button class="bg_black btn3 font_16 lh_30 d_ib white border_radius1">По дате добавления</button>
                <button class="bg_black btn3 font_16 lh_30 d_ib white border_radius2 ml6">По количеству голосов</button>
            </div>
            <div class="container_12">
                <div class="row mb20">
                    <div class="grid_3">
                        <div class="gallery_item al_center p_rel">
                            <div class="gallery_item_bg bg_gray p_abs"></div>
                            <form class="p_rel">
                                <div class="select_file">
                                    <figure class="pb65">
                                        <span class="file_notselected d_ib pb10"></span>
                                        <figcaption class="d_ib  white font_14 pragmaticabook">Загрузитe свое фото</figcaption>
                                    </figure>
                                    <input type="file" class="d_ib" value="Выбрать файл">
                                    <button type="button" class="btn4 bg_red white d_bl font_14">Выбрать фото</button>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="gallery_member_item bg_white bxsh1">
                            <figure class="al_center p_rel">
                                <div class="gallery_member_img mb32 p_rel popup_open" data-modal="#member_itemModal">
                                    <img src="images/gallery_member2.jpg" alt="picture">
                                    <div class="member_img_black p_abs al_center">
                                        <span class="white d_ib font_14 lh_214">Вы проголосовали!</span>
                                    </div>
                                </div>
                                <button class="heart p_abs al_center">
                                   <span class="gallery_member_number d_ib black font_14">145</span>
                                </button>
                                <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Екатерина Шестакович</figcaption>
                            </figure>
                        </div>
                        <div style="display: none;">
                            <div class="box-modal bg_white" id="member_itemModal">
                                <div class="box-modal_close arcticmodal-close al_right">
                                    <span class="arcticmodal-close_x d_ib"></span>
                                </div>
                                <h3 class="title5 al_center pb26 modal_member_title metaM">Участник 
                                    <span class="d_ib red">#132</span>
                                </h3>
                                <figure>
                                    <div class="al_right p_rel">
                                        <div class="modal_member_img bg_black">
                                            <img src="images/modal_member1.jpg" height="495" width="700" alt="">
                                            <div class="member_img_black p_abs al_center">
                                                <span class="white d_ib font_26 member_img_middle">
                                                    <span class="d_ib">Вы проголосовали!</span>
                                                </span>
                                            </div>
                                            <button class="heart_big p_abs al_center" type="button">
                                                <span class="gallery_member_number d_ib black font_22">65</span>
                                            </button>
                                        </div>
                                        <span class="d_ib gray2 pragmaticaLight font_14 lh_30 modal_time">Добавлено <time datetime="2004-07-24T18:18">23.09.2014 в 19:34</time></span>
                                    </div>
                                    <figcaption class="al_center pt20">
                                        <h3 class="pragmaticaLight gallery_member_caption font_24 black lh_30 mb20">Екатерина Шестакович</h3>
                                        <quote class="pragmaticaLight d_bl gray1 mb20 member_item_quote">«Хочу попасть в Книгу рекордов Гиннеса как первый человек, пробежавший марафон на 15-сантиметровых шпильках. Голосуйте за меня!»</quote>
                                        <div class="al_center">
                                            <a href="#" class="modal_soc_f d_ib"></a>
                                            <a href="#" class="modal_soc_b d_ib"></a>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="gallery_member_item bg_white bxsh1">
                            <figure class="al_center p_rel">
                                <div class="gallery_member_img mb32 p_rel">
                                    <img src="images/gallery_member1.jpg" alt="picture">
                                    <div class="member_img_black p_abs al_center">
                                        <span class="white d_ib font_14 lh_214">Вы проголосовали!</span>
                                    </div>
                                </div>
                                <button class="heart p_abs al_center">
                                   <span class="gallery_member_number d_ib black font_14">100</span>
                                </button>
                                <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Анастасия Французова</figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="gallery_member_item bg_white bxsh1">
                            <figure class="al_center p_rel">
                                <div class="gallery_member_img mb32 p_rel">
                                    <img src="images/gallery_member3.jpg" alt="picture">
                                    <div class="member_img_black p_abs al_center">
                                        <span class="white d_ib font_14 lh_214">Вы проголосовали!</span>
                                    </div>
                                </div>
                                <button class="heart p_abs al_center">
                                  <span class="gallery_member_number d_ib black font_14">23</span>
                                </button>
                                <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Юлия Пилявко</figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="row mb20">
                    <div class="grid_3">
                        <div class="gallery_member_item bg_white bxsh1">
                            <figure class="al_center p_rel">
                                <div class="gallery_member_img mb32 p_rel">
                                    <img src="images/gallery_member4.jpg" alt="picture">
                                    <div class="member_img_black p_abs al_center">
                                        <span class="white d_ib font_14 lh_214">Вы проголосовали!</span>
                                    </div>
                                </div>
                                <button class="heart p_abs al_center">
                                  <span class="gallery_member_number d_ib black font_14">65</span>
                                </button>
                                <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Мария Бронько</figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="gallery_member_item bg_white bxsh1">
                            <figure class="al_center p_rel">
                                <div class="gallery_member_img mb32 p_rel">
                                    <img src="images/gallery_member5.jpg" alt="picture">
                                    <div class="member_img_black p_abs al_center">
                                        <span class="white d_ib font_14 lh_214">Вы проголосовали!</span>
                                    </div>
                                </div>
                                <button class="heart p_abs">
                                   <span class="gallery_member_number d_ib black font_14">27</span>
                                </button>
                                <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Светлана Дроздова</figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="gallery_member_item bg_white bxsh1">
                            <figure class="al_center p_rel">
                                <div class="gallery_member_img mb32 p_rel">
                                    <img src="images/gallery_member6.jpg" alt="picture">
                                    <div class="member_img_black p_abs al_center">
                                        <span class="white d_ib font_14 lh_214">Вы проголосовали!</span>
                                    </div>
                                </div>
                                <button class="heart p_abs al_center">
                                    <span class="gallery_member_number d_ib black font_14">72</span>
                                </button>
                                <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Виктория Волосевич</figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="gallery_member_item bg_white bxsh1">
                            <figure class="al_center p_rel">
                                <div class="gallery_member_img mb32 p_rel">
                                    <img src="images/gallery_member7.jpg" alt="picture">
                                    <div class="member_img_black p_abs al_center">
                                        <span class="white d_ib font_14 lh_214">Вы проголосовали!</span>
                                    </div>
                                </div>
                                <button class="heart p_abs al_center">
                                   <span class="gallery_member_number d_ib black font_14">29</span>
                                </button>
                                <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Ирина Зуборевич</figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="row mb20">
                    <div class="grid_3">
                        <div class="gallery_member_item bg_white bxsh1">
                            <figure class="al_center p_rel">
                                <div class="gallery_member_img mb32 p_rel">
                                    <img src="images/gallery_member8.jpg" alt="picture">
                                    <div class="member_img_black p_abs al_center">
                                        <span class="white d_ib font_14 lh_214">Вы проголосовали!</span>
                                    </div>
                                </div>
                                <button class="heart p_abs al_center">
                                    <span class="gallery_member_number d_ib black font_14">82</span>
                                </button>
                                <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Зоя Укупова</figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="gallery_member_item bg_white bxsh1">
                            <figure class="al_center p_rel">
                                <div class="gallery_member_img mb32 p_rel">
                                    <img src="images/gallery_member9.jpg" alt="picture">
                                    <div class="member_img_black p_abs al_center">
                                        <span class="white d_ib font_14 lh_214">Вы проголосовали!</span>
                                    </div>
                                </div>
                                <button class="heart p_abs al_center">
                                    <span class="gallery_member_number d_ib black font_14">32</span>
                                </button>
                                <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Наталья Познякова</figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="gallery_member_item bg_white bxsh1">
                            <figure class="al_center p_rel">
                                <div class="gallery_member_img mb32 p_rel">
                                    <img src="images/gallery_member10.jpg" alt="picture">
                                    <div class="member_img_black p_abs al_center">
                                        <span class="white d_ib font_14 lh_214">Вы проголосовали!</span>
                                    </div>
                                </div>
                                <button class="heart p_abs al_center">
                                    <span class="gallery_member_number d_ib black font_14">19</span>
                                </button>
                                <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Дарья Мустакевич</figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="grid_3">
                        <div class="gallery_member_item bg_white bxsh1">
                            <figure class="al_center p_rel">
                                <div class="gallery_member_img mb32 p_rel">
                                    <img src="images/gallery_member11.jpg" alt="picture">
                                    <div class="member_img_black p_abs al_center">
                                        <span class="white d_ib font_14 lh_214">Вы проголосовали!</span>
                                    </div>
                                </div>
                                <button class="heart p_abs al_center">
                                    <span class="gallery_member_number d_ib black font_14">48</span>
                                </button>
                                <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Ксения Кузловка</figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="row pb50">
                    <div class="grid_6 prefix_3">
                        <button class="btn2 gray font_14 lh_34">Загрузить больше фотографий участников</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--============================== Footer ==============================-->
    <?php include("main_blocks/footer.php") ?>
</body>
</html>