$(function(){
  $.fn.extend({

    openDropdown : function(){

      return this.each(function(){

        var _this = $(this),
          dropdown = $(_this.data('open-dropdown')),
          showClass = dropdown.data('show'),
          hideClass = dropdown.data('hide');

        _this.on('click', function(){

          $(this).toggleClass('active');

          if(dropdown.hasClass(showClass)){
            dropdown.removeClass(showClass).addClass(hideClass);
            setTimeout(function(){
              dropdown.removeClass(hideClass + ' visible');
            },500);
          }
          else{
            dropdown.addClass(showClass + ' visible');
          }
          
        });

      });

  }})

// IPad/IPhone
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
	// Menu Android
	if(window.orientation!=undefined){
    var regM = /ipod|ipad|iphone/gi,
     result = ua.match(regM)
    if(!result) {
     $('.sf-menu li').each(function(){
      if($(">ul", this)[0]){
       $(">a", this).toggle(
        function(){
         return false;
        },
        function(){
         window.location.href = $(this).attr("href");
        }
       );
      } 
     })
    }
   } 

 

});
$('input[value],textarea').each(function(){
                        
      var self = $(this);
    
                        if(self.get(0).nodeName == "TEXTAREA"){
                            var ph = self.text();
                            self.on('focus',function(){
                                if($(this).text() == ph){
                                    $(this).text("");
                                }
                            }).on('blur',function(){
                                if($(this).text() == ""){
                                    $(this).text(mp);
                                }
                            });
                        }
    else{
          var ph = self.attr('value');
        self.on('focus',function(){
            if($(this).val() == ph){
                $(this).val("");
            }
        }).on('blur',function(){
            if($(this).val() == ""){
                $(this).val(ph
                  );
            }
        });
    }
  });
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

