<!DOCTYPE html>
<html lang="en">
<head>
    <title>Error404</title>
    <meta name = "format-detection" content = "telephone=no" />
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/html5.js"></script>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->

    <!--[if lt IE 9]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->
</head>
<body class="content_top2">
    <!--==============================content===========================-->
    <div class="pt25">
        <div class="main">
            <div role="banner" class="logo mr_auto">
                <a href="index.php">Alba</a>
            </div>
            <img class="mr_auto d_bl pt45" src="images/error404.png" height="252" width="597" alt="">
            <p class="al_center pt25 title2 black">
                Страница которую вы ищите, не существуют либо устарела.
            </p>
            <div class="participate_button pt10 pb25 al_center">
                <button class="d_ib  metaM participat_btn">Перейти к конкурсу</button>
            </div>
        </div>
    </div>
</body>
</html>