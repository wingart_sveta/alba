<!DOCTYPE html>
<html lang="en">
<head>
    <title>Final</title>
    <meta name = "format-detection" content = "telephone=no" />
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/html5.js"></script>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->

    <!--[if lt IE 9]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->
</head>
<body>
    <!--==============================content===========================-->
    <div class="content_top2">
        <div class="main">
            <!--==============================header===========================-->
                <?php include("main_blocks/header.php") ?>
            <!--==============================header end===========================-->
            <h1 class="title1 black pt15 reg al_center metaM contest_is_over">
                <img  src="images/contest_before.png" height="80" width="67" alt="">
                    Конкурс окончен
                <img  src="images/contest_after.png" height="80" width="67" alt="">
            </h1>
            <div class="p_rel participation pt37">
                <img class="mr_auto content_img2" src="images/content_img.png" height="412" width="959" alt="">
            </div>
            <div class="tourist_certificate pb50 clearfix">
                <h4 class="title8 black pt10 al_center metaM tourist_certificate_h4">
                    <img class="pt16"  src="images/tourist_before.png" height="25" width="35" alt="">
                        Туристический сертификат
                    <img class="pt16"  src="images/tourist_after.png" height="25" width="35" alt="">
                </h4>
                <p class="tourist2 al_center title2 black pragmaticaLight">
                    Поздравляем Анастасию с победой, её фото набрало наибольшее количество голосов и она получает главный приз — подарочный туристический сертификат на сумму 50 000 руб.
                </p>
                <div class="gallery_member_item mr_auto bg_white bxsh1">
                    <figure class="al_center p_rel">
                        <div class="gallery_member_img mb32 p_rel">
                            <img src="images/gallery_member4.jpg" alt="picture">
                        </div>
                        <button class="heart_final p_abs al_center">
                            <span class="gallery_member_number d_ib black font_14">1000</span>
                        </button>
                        <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Мария Бронько</figcaption>
                    </figure>
                </div>
            </div>
        </div>
        <div class="content_top3 brdb_dashed brdt_dashed">
            <div class="container_12 lucky_beggar mb50">
                <h4 class="title8 black pt30 al_center metaM tourist_certificate_h4">
                    <img class="pt16"  src="images/tourist_before.png" height="25" width="35" alt="">
                        Планшеты Lenovo A10
                    <img class="pt16"  src="images/tourist_after.png" height="25" width="35" alt="">
                </h4>
                <p class="lucky2 al_center title2 black pragmaticaLight">
                    Четверо счастливчиков, набравших достойное количество голосов, стали обладателями планшетов «Lenovo A10».
                </p>
                <div class="row">
                        <div class="grid_3">
                            <div class="gallery_member_item bg_white bxsh1">
                                <figure class="al_center p_rel">
                                    <div class="gallery_member_img mb32 p_rel">
                                        <img src="images/gallery_member4.jpg" alt="picture">
                                    </div>
                                    <button class="heart_final p_abs al_center">
                                      <span class="gallery_member_number d_ib black font_14">65</span>
                                    </button>
                                    <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Мария Бронько</figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="gallery_member_item bg_white bxsh1">
                                <figure class="al_center p_rel">
                                    <div class="gallery_member_img mb32 p_rel">
                                        <img src="images/gallery_member5.jpg" alt="picture">
                                    </div>
                                    <button class="heart_final p_abs">
                                       <span class="gallery_member_number d_ib black font_14">27</span>
                                    </button>
                                    <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Светлана Дроздова</figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="gallery_member_item bg_white bxsh1">
                                <figure class="al_center p_rel">
                                    <div class="gallery_member_img mb32 p_rel">
                                        <img src="images/gallery_member6.jpg" alt="picture">
                                    </div>
                                    <button class="heart_final p_abs al_center">
                                        <span class="gallery_member_number d_ib black font_14">72</span>
                                    </button>
                                    <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Виктория Волосевич</figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="grid_3">
                            <div class="gallery_member_item bg_white bxsh1">
                                <figure class="al_center p_rel">
                                    <div class="gallery_member_img mb32 p_rel">
                                        <img src="images/gallery_member7.jpg" alt="picture">
                                    </div>
                                    <button class="heart_final p_abs al_center">
                                       <span class="gallery_member_number d_ib black font_14">29</span>
                                    </button>
                                    <figcaption class="pragmaticaLight gallery_member_caption font_16 black lh_30">Ирина Зуборевич</figcaption>
                                </figure>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="content_top4 brdb_dashed">
            <div class="main gift_certificate clearfix mb50">
                <h4 class="title8 black pt30 al_center metaM tourist_certificate_h4">
                    <img class="pt16"  src="images/tourist_before.png" height="25" width="35" alt="">
                        Подарочные сертификаты ALBA
                    <img class="pt16"  src="images/tourist_after.png" height="25" width="35" alt="">
                </h4>
                <p class="lucky2 al_center title2 black pragmaticaLight">
                    Обладатели подарочных сертификатов для реализации в салонах <a href="#" class="lk1 lh_31 metaM">ALBA</a>
                </p>
                <div class="money1">
                    <h5 class="font_18 lh_36 black metam reg">На сумму 5000 Руб.</h5>
                    <span class="sertific"></span>
                    <ul class="money_item">
                        <li class="money_name black pragmaticabook lh_36 font_18">Наталья Хохлова</li>
                        <li class="money_name black pragmaticabook lh_36 font_18">Людмила Гроздупова</li>
                        <li class="money_name black pragmaticabook lh_36 font_18">Валентина Перекол</li>
                        <li class="money_name black pragmaticabook lh_36 font_18">Анастасия Володько</li>
                        <li class="money_name black pragmaticabook lh_36 font_18">Галина Мортынюк</li>
                    </ul>
                </div>
                <div class="money1">
                    <h5 class="font_18 lh_36 black metam reg">На сумму 3000 Руб.</h5>
                    <span class="sertific"></span>
                    <ul class="money_item">
                        <li class="money_name black pragmaticabook lh_36 font_18">Юлия Смирнова</li>
                        <li class="money_name black pragmaticabook lh_36 font_18">Оксана Афанасенко</li>
                        <li class="money_name black pragmaticabook lh_36 font_18">Маргарита Осипович</li>
                        <li class="money_name black pragmaticabook lh_36 font_18">Екатерина Шоповалова</li>
                        <li class="money_name black pragmaticabook lh_36 font_18">Светлана Баровая</li>
                    </ul>
                </div>
                <div class="money1">
                    <h5 class="font_18 lh_36 black metam reg">На сумму 1000 Руб.</h5>
                    <span class="sertific"></span>
                    <ul class="money_item">
                        <li class="money_name black pragmaticabook lh_36 font_18">Наталья Хохлова</li>
                        <li class="money_name black pragmaticabook lh_36 font_18">Людмила Гроздупова</li>
                        <li class="money_name black pragmaticabook lh_36 font_18">Валентина Перекол</li>
                        <li class="money_name black pragmaticabook lh_36 font_18">Анастасия Володько</li>
                        <li class="money_name black pragmaticabook lh_36 font_18">Галина Мортынюк</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content_top5 brdb_dashed">
            <div class="main cupon_final clearfix mb50">
                <h4 class="title8 black pt30 al_center metaM tourist_certificate_h4">
                    <img class="pt16"  src="images/tourist_before.png" height="25" width="35" alt="">
                        Купоны на скидку 1000 руб. в интернет-магазине
                    <img class="pt16"  src="images/tourist_after.png" height="25" width="35" alt="">
                </h4>
                <p class="cupon2_p al_center title2 black pragmaticaLight">
                    Обладатели купонов на скидку 1000 руб. на покупку в <a href="#" class="lk1 lh_31 metaM">интернет магазине ALBA</a>
                </p>
                <div class="cupon1">
                    <ul class="cupon_item">
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Наталья Хохлова</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Людмила Гроздупова</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Валентина Перекол</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Анастасия Володько</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Галина Мортынюк</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Наталья Хохлова</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Людмила Гроздупова</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Валентина Перекол</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Анастасия Володько</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Галина Мортынюк</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Наталья Хохлова</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Людмила Гроздупова</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Валентина Перекол</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Анастасия Володько</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Галина Мортынюк</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Наталья Хохлова</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Людмила Гроздупова</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Валентина Перекол</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Анастасия Володько</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Галина Мортынюк</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Наталья Хохлова</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Людмила Гроздупова</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Валентина Перекол</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Анастасия Володько</li>
                        <li class="cupon_name black pragmaticabook lh_36 font_18">Галина Мортынюк</li>
                    </ul>
                </div>
                <div class="cupon2">
                    <ul class="cupon_item2">
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Наталья Хохлова</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Людмила Гроздупова</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Валентина Перекол</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Анастасия Володько</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Галина Мортынюк</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Наталья Хохлова</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Людмила Гроздупова</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Валентина Перекол</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Анастасия Володько</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Галина Мортынюк</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Наталья Хохлова</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Людмила Гроздупова</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Валентина Перекол</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Анастасия Володько</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Галина Мортынюк</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Наталья Хохлова</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Людмила Гроздупова</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Валентина Перекол</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Анастасия Володько</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Галина Мортынюк</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Наталья Хохлова</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Людмила Гроздупова</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Валентина Перекол</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Анастасия Володько</li>
                        <li class="cupon_name2 black pragmaticabook lh_36 font_18">Галина Мортынюк</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content_top6">
            <div class="main mb50">
                <h4 class="title8 black pt30 pb35 al_center metaM tourist_certificate_h4">
                    <img class="pt16"  src="images/tourist_before.png" height="25" width="35" alt="">
                        Видео розыгрыша
                    <img class="pt16"  src="images/tourist_after.png" height="25" width="35" alt="">
                </h4>
                <div class="video_final mr_auto">
                    <iframe width="660" height="409" src="//www.youtube.com/embed/iCvhpYSh93M" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--============================== Footer ==============================-->
    <?php include("main_blocks/footer.php") ?>
</body>
</html>