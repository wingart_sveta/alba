<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home</title>
    <meta name = "format-detection" content = "telephone=no" />
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/html5.js"></script>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>

    <![endif]-->

    <!--[if lt IE 9]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
     <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <![endif]-->
</head>
<body>
    <!--==============================content===========================-->
    <div class="contentmain">
        <div class="content_top ">
            <div class="main">
            <!--==============================header===========================-->
                <?php include("main_blocks/header.php") ?>
            <!--==============================header end===========================-->
                <h1 class="title1 reg al_center metaM">В Италию с <span class="metaBlack">Alba</span></h1>
                <p class="prizes al_center pragmaticaLight pt5 title2 black">
                    Совершайте покупки и получите шанс выиграть поездку в Италию и множество других     ценных <a class="lk1 pragmaticaMedium" href="#">призов!</a>
                </p>
                <div class="p_rel participation">
                    <img class="mr_auto content_img" src="images/content_img.png" height="412" width="959" alt="">
                    <a class="plus1 plus p_abs" href="#"></a>
                    <span class="plus_hov plus_hov1 p_abs pragmaticabook">
                      Купоны на скидку 1000 руб.<br>в интернет-магазине
                    </span>
                    <a class="plus2 plus p_abs" href="javascript:;"></a>
                    <span class="plus_hov plus_hov2 p_abs pragmaticabook">
                      Подарочный туристический<br> сертификат на сумму<br> 50 000 руб
                    </span>
                    <a class="plus3 plus p_abs" href="#"></a>
                    <span class="plus_hov plus_hov3 p_abs pragmaticabook">
                      Подарочные сертификаты
                    </span>
                    <a class="plus4 plus p_abs" href="#"></a>
                    <span class="plus_hov plus_hov4 p_abs pragmaticabook">
                      Планшеты Lenovo A10
                    </span>
                    <div class=" vk p_abs socicon mb20">
                      <a href="#" class="socicon_lk d_ib al_center">
                        <span class="disc_b d_ib"></span>
                        <span  class="flow font_18 lh_30 d_bl">&laquo;Расскажи друзьям&raquo;</span>
                      </a>
                    </div>
                    <div class="faceb p_abs socicon mb20">
                      <a href="#" class="socicon_lk d_ib al_center">
                        <span class="disc_f d_ib"></span>
                        <span  class="flow font_18 lh_30 d_bl">&laquo;Поделись со всеми&raquo;</span>
                      </a>
                    </div>
                </div>
                <div class="participate_button al_center">
                    <button class="d_ib metaM participat_btn">Принять участие</button>
                </div>
            </div>
        </div>
        <div class="content_middle">
            <div class="main clearfix">
                <div class="conditions_competition mt100  f_left p_rel">
                        <h4 class="title3 white pt27 pb40 reg al_center metaM">Условия конкурса</h4>
                        <span class="cond_span"></span>
                        <p class="competition_text pragmaticaLight title4 white">
                            Совершите покупку на сумму не менее 7 000 руб. в <a class="lk1 pragmaticaMedium" href="#">салонах</a> либо в <a class="lk1 pragmaticaMedium" href="#">интернет-магазине ALBA</a> в период с 1 ноября 2014 года по 30 ноября 2014 года (в акции участвует товар TM ALBA из ассортимента обуви или сумок) 
                        </p>
                        <span class="cond_span"></span>
                        <p class="competition_text pragmaticaLight title4 white">
                            Заполните короткую анкету и зарегистрируйте на сайте чек либо номер своего заказа в <a class="lk1 pragmaticaMedium" href="#">интернет-магазине ALBA</a>.
                        </p>
                        <span class="cond_span"></span>
                        <p class="competition_text pragmaticaLight title4 white">
                            Участвуйте в розыгрыше призов от ALBA.
                        </p>
                        <span class="cond_span"></span>
                        <p class="competition_text pragmaticaLight title4 white">
                            Загрузите фото покупки, собирайте лайки (на промо-сайте) и увеличивайте свои шансы на выигрыш!
                        </p>
                        <div class="competition_button p_abs">
                            <button class="d_bl mr_auto metaM competition_btn">Подробнее</button>
                        </div>
                </div>
                <div class="social_main p_rel mt165 f_left">
                    <img class="p_abs" src="images/bxsh_social.png" height="340" width="18" alt="">
                    <h4 class="title3 black pt92 pb24 reg al_center metaM">Мы в Соцсетях</h4>
                    <div class="soc_icon clearfix">
                        <a href="#" class="f_left soc_icon_item soc_vk"></a>
                        <a href="#" class="f_left soc_icon_item soc_fcb"></a>
                        <a href="#" class="f_left soc_icon_item soc_inst"></a>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <!--============================== Footer ==============================-->
    <?php include("main_blocks/footer.php") ?>
</body>
</html>