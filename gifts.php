<!DOCTYPE html>
<html lang="en">
<head>
    <title>Gifts</title>
    <meta name = "format-detection" content = "telephone=no" />
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/html5.js"></script>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->

    <!--[if lt IE 9]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->
</head>
<body>
    <!--==============================content===========================-->
    <div class="content_top2">
        <div class="main">
        <!--==============================header===========================-->
            <?php include("main_blocks/header.php") ?>
        <!--==============================header end===========================-->
            <h1 class="title6 black pt1 reg al_center metaM">Призы</h1>
            <p class="prizes2 al_center pt7 title2 black pragmaticaLight">
                Участвуйте в фотоконкурсе и получите шанс выиграть один из ценных призов!
            </p>
            <div class="p_rel participation pt37">
                <img class="mr_auto content_img2" src="images/content_img.png" height="412" width="959" alt="">
            </div>
            <div class="gifts pb50 pt25 clearfix">
                <ul class="gifts_item ">
                    <li class="gifts_li al_left prizes3 pt7 title2 black pragmaticaLight"><i class="pr30">•</i> Подарочный туристический сертификат на сумму 50 000 руб.</li>
                    <li class="gifts_li al_left prizes3 pt7 title2 black pragmaticaLight"><i class="pr30">•</i> 4 планшета Lenovo A10</li>
                    <li class="gifts_li al_left prizes3 pt7 title2 black pragmaticaLight"><i class="pr30">•</i> 5 подарочных сертификатов ALBA на 5 000 руб.</li>
                    <li class="gifts_li al_left prizes3 pt7 title2 black pragmaticaLight"><i class="pr30">•</i> 5 подарочных сертификатов ALBA на 3 000 руб.</li>
                    <li class="gifts_li al_left prizes3 pt7 title2 black pragmaticaLight"><i class="pr30">•</i> 5 подарочных сертификатов ALBA на 1 000 руб.</li>
                    <li class="gifts_li al_left prizes3 pt7 title2 black pragmaticaLight"><i class="pr30">•</i> 50 купонов на скидку 1 000 руб. в интернет-магазине</li>
                </ul>
            </div>
        </div>
    </div>
    <!--============================== Footer ==============================-->
    <?php include("main_blocks/footer.php") ?>
</body>
</html>