<!DOCTYPE html>
<html lang="en">
<head>
    <title>Terms</title>
    <meta name = "format-detection" content = "telephone=no" />
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/html5.js"></script>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->

    <!--[if lt IE 9]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->
</head>
<body>
    <!--==============================content===========================-->
    <div class="content_top2">
        <div class="main">
        <!--==============================header===========================-->
            <?php include("main_blocks/header.php") ?>
        <!--==============================header end===========================-->
            <h1 class="title6 black pt1 reg al_center metaM">Правила</h1>
            <div class="terms pb50 pt25 clearfix">
                <ul class="terms_ul al_left">
                    <li class="terms_li lh_30 black pragmaticaLight d_bl">
                         Совершите покупку на сумму не менее 7 000 руб. в <a class="lk1 pragmaticaMedium" href="#">салонах</a> либо в <a class="lk1 pragmaticaMedium" href="#">интернет-магазине ALBA</a> в период с 1 ноября 2014 года по 30 ноября 2014 года (в акции участвует только товар TM ALBA из ассортимента обуви или сумок)
                    </li>
                    <li class="terms_li lh_30 black pragmaticaLight d_bl">
                         Заполните короткую анкету и зарегистрируйте на сайте чек либо номер своего заказа в <a class="lk1 pragmaticaMedium" href="#">интернет-магазине ALBA.</a> 
                    </li>
                    <li class="terms_li lh_30 black pragmaticaLight d_bl">
                         Участвуйте в розыгрыше призов от ALBA.
                    </li>
                    <li class="terms_li lh_30 black pragmaticaLight d_bl">
                         Загрузите фото покупки, собирайте лайки (на промо-сайте) и увеличивайте свои шансы на выигрыш!
                    </li>
                </ul>
                <p class="black pragmaticaLight mt30 mb30">
                    Определение первых пяти победителей происходит путем открытого голосования на Сайте конкурса в период с 1 ноября по 30 ноября 2014 г. Остальные победители определяются с помощью сервиса <a class="lk1 pragmaticaMedium" href="http://randstuff.ru.">http://randstuff.ru.</a>
                </p>
                <p class="black pragmaticaLight mb30">
                    Подведение итогов, проверка выполнения условий Конкурса Участниками, претендующими на получение Приза, происходит в период с 1 декабря по 15 декабря 2014 г.
                </p>
                <p class="black pragmaticaLight mb30">
                    Вручение призов и оформление всех необходимых документов осуществляется в период с 16 по 31 декабря 2014 года.
                </p>
                <div class="terms_button">
                    <button type="button" class="pragmaticabook let1 font_14 btn4 bg_red white">Полный перечень правил в PDF</button>
                </div>
            </div>
        </div>
    </div>
    <!--============================== Footer ==============================-->
    <?php include("main_blocks/footer.php") ?>
</body>
</html>