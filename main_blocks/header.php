<header class="clearfix main">
    <div role="banner" class="logo f_left">
        <a href="index.php">Alba</a>
    </div>
    <div class="clearfix link_main metaB f_left">
        <ul class="link_main_ul">
            <li class="f_left header_link d_ib"><a class="header_link_a" href="terms.php">Условия конкурса</a></li>
            <li class="f_left header_link d_ib"><a class="header_link_a current" href="gallery.php">Галерея участников</a></li>
            <li class="f_left header_link d_ib"><a class="header_link_a" href="gifts.php">Призы</a></li>
            <li class="f_left header_link d_ib"><a class="header_link_a popup_open" href="javascript:;" data-modal="#sign_in">Войти</a></li>
        </ul>
    </div>
    <div class="nav_menu f_left">
        <ul class="menu pragmaticaLight clearfix">
            <li class="menu_item">
                <a class="menu_link" href="#">ALBA</a>
            </li>
            <li class="menu_item">
                <a class="menu_link" href="#">Lookbook</a>
            </li>
            <li class="menu_item">
                <a class="menu_link" href="#">Коллекции</a>
            </li>
            <li class="menu_item">
                <a class="menu_link" href="#">Наши салоны</a>
            </li>
            <li class="menu_item">
                <a class="menu_link" href="#">Интернет-магазин</a>
            </li>
        </ul>
    </div>
</header>


<!-- ==============================sign_in_popup =================================-->
<div style="display: none;">
    <div class="box-modal bg_white p_rel" id="sign_in">
        <div>
            <div class="box-modal_close arcticmodal-close al_right">
                <span class="arcticmodal-close_x d_ib"></span>
            </div>
            <h3 class="title5 al_center pb26 reg black modal_member_title">Вход</h3>
        </div>
        <span class="sign_brd"></span>
        <div class="bg_gray2 bg_sign_input">
            <div class="input_sign">
                <label class="d_bl black title4" for="email">Email:</label>
                <input type="email" name="email">
                <label class="d_bl pt15 black title4" for="password">Пароль:</label>
                <input type="password" name="password">
                <span class="registration_tools pragmaticabook font_14 lh_18 let">Проверьте правильность ввода электронной почты и пароля.</span>
                <span class="d_bl pt5"><a href="#" class="lk1 font_14 popup_open" data-modal="#forgot_password">Забыли пароль?</a></span>
                <p class="font_14 pt10 gray2 let1">Незарегестрированы, пройдите <a href="#" class="lk1 popup_open" data-modal="#registration">регистрацию</a>.</p>
            </div>
            <div class="sign_button p_abs">
                <button class="d_bl mr_auto metaM sign_btn">Войти</button>
            </div>
        </div>
    </div>
</div>
<!-- ==============================sign_in_popup end=================================-->

<!-- ==============================forgot_password_popup =================================-->
<div style="display: none;">
    <div class="box-modal bg_white p_rel" id="forgot_password">
        <div>
            <div class="box-modal_close arcticmodal-close al_right">
                <span class="arcticmodal-close_x d_ib"></span>
            </div>
            <h3 class="title5 al_center pb26 reg black modal_member_title">Забыли пароль?</h3>
        </div>
        <span class="forgot_brd"></span>
        <div class="bg_gray2 bg_input">
            <div class="input_forgot">
                <label class="d_bl black title4" for="email">Email:</label>
                <input type="email" name="email">
                <span class="registration_tools pragmaticabook font_14 lh_18 let pt5">Такая почта не зарегистрирована</span>
            </div>

            <div class="forgot_button p_abs"> 
                <button class="d_bl mr_auto metaM forgot_btn popup_open" data-modal="#restored">Отправить</button>
            </div>
        </div>
    </div>
</div>
<!-- ==============================forgot_password_popup end=================================-->

<!-- ==============================restored_popup =================================-->
<div style="display: none;">
    <div class="box-modal bg_gray2 p_rel" id="restored">
        <p class="font_14 black al_center pt30 pragmaticabook mb0">Письмо с инструкциями по восстановлению пароля отправлено вам на почту.</p>
    </div>
</div>
<!-- ==============================restored_popup end=================================-->

<!-- ==============================registration_popup =================================-->
<div style="display: none;">
    <div class="box-modal bg_white p_rel" id="registration">
        <div>
            <div class="box-modal_close arcticmodal-close al_right">
                <span class="arcticmodal-close_x d_ib"></span>
            </div>
            <h3 class="title5 al_center pb26 reg black modal_member_title">Регистрация</h3>
        </div>
        <span class="forgot_brd"></span>
        <div class="bg_gray2 bg_reg_input">
           
            <p class="font_14 gray2 pragmaticaLight lh_24 mb8 mt5 ml30">1. Информация об участнике</p>
             <div class="input_reg clearfix">
                <div class="f_left d_ib">
                    <label class="d_bl black title4" for="">Имя:</label>
                    <input type="text" name="">
                    <label class="d_bl black title4 pt20" for="email">Email:</label>
                    <input type="email" name="email">
                </div>
                <div class="f_right d_ib">
                    <label class="d_bl black title4" for="">Фамилия:</label>
                    <input type="text" name="">
                    <label class="d_bl black title4 pt20" for="">Телефоны:</label>
                    <input type="text" name="" value="+7 (     )">
                </div>
                <span class="registration_tools pragmaticabook font_14 lh_18 pt5">Проверьте правильность ввода электронной почты и телефона.</span>
                <span class="registration_tools pragmaticabook font_14 lh_18 pt5">Все поля обязательны для заполнени</span>
            </div>
            <div class="information_reg">
                <p class="font_14 gray2 pragmaticaLight lh_24 mb8 mt5">2. Информация о покупке</p>
                <span class="title4 black d_bl pb16">Место покупки:</span>
                <div class="radio_place">
                    <div class="f_left radio_place2">
                        <input class="d_ib" type="radio" name="place_item" id="salon">
                        <label class="place_of_purchase d_ib pragmaticabook black font_14 mb10" for="salon">Салон ALBA</label>
                        <input class="d_ib" type="radio" name="place_item" id="magazine">
                        <label class="place_of_purchase pragmaticabook black font_14 d_bl" for="magazine">Интернет магазин</label>
                    </div>
                    <div class="f_right">
                        <select class="styler reg_select d_ib" name="" id="">
                            <option value="Салон Бум, Москва, ул. Перерва 43">Салон Бум, Москва, ул. Перерва 43</option>
                            <option value="Салон Бум, Москва, ул. Перерва 43">Салон Бум, Москва, ул. Перерва 43</option>
                            <option value="Салон Бум, Москва, ул. Перерва 43">Салон Бум, Москва, ул. Перерва 43</option>
                        </select>
                    </div>
                  </div>
            </div>
            <div class="number_reg p_rel">
                <span class="title4 black d_bl pb16">Номер 
                    <a href="#" class="lk2 checker">чека
                        <span class="reg_check p_abs"><img src="images/reg_check.png" alt=""></span>
                    </a> или <a href="#" class="lk2">заказа</a>:
                </span>
                <input type="text">
                <span class="pragmaticabook font_14 lh_18 d_bl gray2 pt8">
                    * в конкурсе участвуют чеки, в которых присутствует товар из ассортимента обуви и/или сумок : только товар TM ALBA на сумму 7000 рублей и более.
                </span>
                <span class="pragmaticabook font_14 lh_18 d_bl gray2">
                    * в личном кабинете вы можете внести большее количество номеров
                </span>
                <span class="registration_tools pragmaticabook font_14 lh_18 pt5">Все поля обязательны для заполнени</span>
            </div>
            <div class="d_bl pl30 pt30 pr30 pb50">
                <span class="registration_tools ragmaticabook font_14 lh_18 pt5 let">Чтобы пройти регистрацию вы должны подтвердить правила участия в акции.</span>
                <input type="checkbox" id="fiter_input1">
                <label class="filter_label black pragmaticabook font_14 lh_18" for="fiter_input1">
                    Я подтверждаю достоверность указанных данных, а также принимаю правила акции «В Италию с ALBA» и политику конфиденциальности.
              </label>
            </div>
            <div class="forgot_button p_abs"> 
                <button class="d_bl mr_auto metaM forgot_btn">Зарегистрировать</button>
            </div>
        </div>
    </div>
</div>
<!-- ==============================registration_popup end=================================-->
<!-- ==============================restored_popup =================================-->
    <div style="display: none;">
        <div class="box-modal bg_gray2 p_rel" id="save">
            <p class="font_14 black al_center pt20 pb20 pragmaticabook mb0">Ваши данные успешно сохранены.</p>
        </div>
    </div>
    <div style="display: none;">
        <div class="box-modal bg_gray2 p_rel" id="save_check">
            <p class="font_14 black al_center pt20 pb20 pragmaticabook mb0">Ваш чек добавлен в список и отправлен  на модерацию.</p>
        </div>
    </div>
    <!-- ==============================restored_popup end=================================-->
    <!-- ==============================sign_in_popup =================================-->
    <div style="display: none;">
        <div class="box-modal bg_white p_rel" id="delete">
            <div>
                <div class="box-modal_close arcticmodal-close al_right">
                    <span class="arcticmodal-close_x d_ib"></span>
                </div>
                <h3 class="title5 al_center pb20 reg black metaM modal_member_title">Удалить Фото?</h3>
            </div>
            <span class="sign_brd"></span>
            <div class="bg_gray2 bg_delete_input">
               <p class="pragmaticaLight font_16 lh_24 pt15 black al_center">
                   Вы действительно хотите удалить фото? После удаления ваше фото перестанет участвовать в конкурсе, а набранные голоса обнулятся.
               </p>
               <form class="clearfix pt8 pl30 pr30">
                    <button class="bg_red btn5 white font_14 f_left d_ib ">Да, удалить</button>
                    <button type="button" class="btn5 bg_gray3 f_right white d_ib font_14">Нет</button>
                </form>
            </div>
        </div>
    </div>
    <!-- ==============================sign_in_popup end=================================-->

