<footer>
    <div class="main clearfix">
    	<div class="f_left">
    		<div class="footer_link_main metaB ">
    		    <a class="footer_link d_ib f_left" href="terms.php">Условия конкурса</a>
    		    <a class="footer_link d_ib f_left" href=".php">Галерея участников</a>
    		    <a class="footer_link d_ib f_left" href="gifts.php">Призы</a>
    		    <a class="footer_link d_ib f_left popup_open" href="javascript:;" data-modal="#sign_in">Войти</a>
    		</div>
    	</div>
    	<div class="f_right">
    		<div class="call_us white">
    			<span class="call_us_number d_bl">
    				<span class="number_item">8-800-</span>555-00-30
    			</span>
    			<a class="d_bl gmail_write ml18" href="#">voice@the-alba.com</a>
    		</div>
    	</div>
    </div>
</footer>
<!--============================== Scripts inclide and init =================================-->
<script src="js/jquery.formstyler.js"></script>
<script src="js/jquery.arcticmodal-0.3.min.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/main.js"></script>
<script src="js/script.js"></script>


