<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile</title>
  
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/html5.js"></script>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>


    <![endif]-->

    <!--[if lt IE 9]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
       <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
      
    <![endif]-->
</head>
<body>
    <!--==============================content===========================-->
    <div class="content_top2">
        <div class="main">
        <!--==============================header===========================-->
            <?php include("main_blocks/header_1.php") ?>
        <!--==============================header end===========================-->
        <div class="profile bg_white mt13 bxsh1">
            <h2 class="title9 black al_center pt24 pb24 metaM">Анастасия Волошкевич</h2>
            <div class="container_12 mb60">
                <div class="row">
                        <div class="grid_6">
                            <div class="profile_box_foto">
                                <div class="gallery_item bg_gray5 al_center">
                                    <form>
                                        <div class="select_file al_center">
                                            <figure class="pb22">
                                                <span class="file_notselected d_ib pb10"></span>
                                                <figcaption class="d_ib  white font_14 pragmaticabook">Загрузитe фотографию и получите шанс выиграть поездку в Италию!</figcaption>
                                            </figure>
                                            <input type="file" class="d_ib" value="Выбрать файл">
                                            <button type="button" class="btn4 bg_red white d_ib font_14 select_file_btn">Выбрать фото</button>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="profile_box">
                                <span class="d_bl font_14 lh_24 gray2 pragmaticaLight">Информация об участнике</span>
                                <div class="clearfix br_bot pb10 profile_box_contact">
                                    <div class="f_left profile_e_mail">
                                        <span class="d_bl black">E-mail:</span>
                                        <span class="d_bl pragmaticaBook black font_14">anastasia_voloschkevich1985@gmail.com</span>
                                    </div>
                                    <div class="f_right profile_tel">
                                        <span class="d_bl black">Телефон:</span>
                                        <span class="d_bl pragmaticaBook black font_14">+7 (435) 234-61-17</span>
                                    </div>
                                </div>
                                <div class="profile_checks">
                                    <span class="d_bl font_14 lh_24 gray2 pragmaticaLight pb8">Добавьте больше чеков и увеличьте свои шансы на победу.</span>
                                    <div id="tabs">
                                        <ul class="profile_checks_list">
                                            <li class="d_ib pragmaticabook profile_checks_item1 al_center"><a href="#tabs-1" class="profile_checks_lk1 font_14 lh_34">Список чеков</a></li>
                                            <li class="d_ib pragmaticabook profile_checks_item2 al_center">
                                                <a href="#tabs-2" class="profile_checks_lk2 font_14 lh_34">Добавить чек</a>
                                                <button class="add_checks"></button>
                                            </li>
                                        </ul>
                                        <div id="tabs-1" class="p_rel">
                                            <div class="scroll-pane">
                                                <table id="checks" class="">
                                                    <tr>
                                                        <th class="font_16 pragmaticaMedium fw500 black">Место покупки:</th>
                                                        <th class="font_16 pragmaticaMedium fw500 black">Номер чека:</th>
                                                    </tr>
                                                    <tr>
                                                        <td class="">Салон Бум, Москва, ул.Перерва 43</td>
                                                        <td class="check_true p_rel font_18">1948.18
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Салон Бум, Москва, ул.Перерва 43</td>
                                                        <td class="check_felse p_rel font_18">3245.35
                                                            <span class="check_felse_span p_abs pragmaticabook">Чек на проверке</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Салон Бум, Москва, ул.Перерва 43</td>
                                                        <td class="check_true p_rel font_18">2255.32
                                                            <span class="check_true_span p_abs pragmaticabook">Чек одобрен</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Салон Бум, Москва, ул.Перерва 43</td>
                                                        <td class="check_true font_18">2255.32
                                                            <span class="check_true_span p_abs pragmaticabook">Чек одобрен</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Салон Бум, Москва, ул.Перерва 43</td>
                                                        <td class="check_true font_18">2255.32
                                                            <span class="check_true_span p_abs pragmaticabook">Чек одобрен</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Салон Бум, Москва, ул.Перерва 43</td>
                                                        <td class="check_true font_18">2255.32
                                                            <span class="check_true_span p_abs">Чек одобрен</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Салон Бум, Москва, ул.Перерва 43</td>
                                                        <td class="check_true font_18">2255.32
                                                            <span class="check_true_span p_abs">Чек одобрен</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                         </div>
                                        <div id="tabs-2">
                                            <span class="title4 black d_bl pb4">Место покупки:</span>
                                            <form id="profile_form">
                                                <div class="profile_radio_box">
                                                    <div class="d_ib profile_place">
                                                        <input class="d_ib styler1" type="radio" name="place_item" id="salon1" checked="checked">
                                                        <label class="profile_label d_ib pragmaticabook black font_14 lh_18" for="salon1">Салон ALBA</label>
                                                    </div>
                                                    <div class="d_ib profile_place">
                                                        <input class="d_ib styler1" type="radio" name="place_item" id="magazine1">
                                                        <label class="profile_label pragmaticabook black font_14 lh_18 d_ib" for="magazine1">Интернет магазин</label>
                                                    </div>
                                                </div>
                                                <select class="styler reg_select d_ib">
                                                    <option value="Салон Бум, Москва, ул. Перерва 43">Салон Бум, Москва, ул. Перерва 43</option>
                                                    <option value="Салон Бум, Москва, ул. Перерва 43">Салон Бум, Москва, ул. Перерва 43</option>
                                                    <option value="Салон Бум, Москва, ул. Перерва 43">Салон Бум, Москва, ул. Перерва 43</option>
                                                </select>
                                                <div class="profile_number_reg p_rel">
                                                    <span class="title4 black d_bl pb2">   Номер 
                                                        <a href="#" class="lk2 checker">чека
                                                        <span class="reg_check p_abs">
                                                            <img src="images/reg_check.png" alt="">
                                                        </span>
                                                        </a> или <a href="#" class="lk2">заказа</a>:
                                                    </span>
                                                    <input type="text">
                                                    <span class="pragmaticabook font_14 lh_18 d_bl gray2 pt4">
                                                    * в конкурсе участвуют чеки, в которых присутствует товар из ассортимента обуви и/или сумок : только товар TM ALBA на сумму 7000 рублей и более.
                                                    </span>
                                                </div>
                                                <div class="profile_btn">
                                                    <button type="button" class="btn4 white bg_red pragmaticabook black font_14 popup_open" data-modal="#save_check">Добавить</button>
                                                     <div style="display: none;">
                                                        <div class="box-modal bg_gray2 p_rel" id="save_check">
                                                            <p class="font_14 black al_center pt20 pb20 pragmaticabook mb0">Ваш чек добавлен в список и отправлен  на модерацию.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <!--============================== Footer ==============================-->
    <?php include("main_blocks/footer.php") ?>
</body>
</html>