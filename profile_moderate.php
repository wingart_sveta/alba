<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile</title>
    <meta name = "format-detection" content = "telephone=no" />
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/html5.js"></script>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->

    <!--[if lt IE 9]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <![endif]-->
</head>
<body>
    <!--==============================content===========================-->
    <div class="content_top">
        <div class="main">
        <!--==============================header===========================-->
            <?php include("main_blocks/header_1.php") ?>
        <!--==============================header end===========================-->
        <div class="profile bg_white bxsh1 mt13 mb60">
            <h2 class="title9 pt30 pb30 al_center metaM black">Анастасия Волошкевич</h2>
            <div class="container_12">
                <div class="row">
                    <div class="grid_6">
                        <div class="profile_box_foto p_rel">
                            <div class="bg_img_profile al_center">
                                <img src="images/profile_img_add.jpg" height="452" width="489" alt="picture">
                                <div class="member_img_black p_abs al_center">
                                    <span class="d_ib member_img_middle al_center pl20 pr20">
                                        <span class="white d_ib font_18">
                                            <span class="d_ib pb20">Фото находится на стадии проверки.</span>
                                            <span class="d_ib ">Напишите описание к вашему фото и увеличьте свои шансы на победу.</span>
                                        </span>
                                        
                                    </span>
                                </div>
                            </div>
                            <img class="p_abs photo_abs" src="images/heart_gray.png" height="70" width="80" alt="">
                        </div>
                        <div class="add_photo bg_gray2 brdr_dashed3">
                            <span class="pragmaticabook font_14 lh_18 al_center d_bl gray2 pt55 pb16 brdb_dashed2">
                                Добавлено 23.09.2014 в 19:34
                            </span>
                            <div class="description_photo clearfix">
                                <span class="title4 black pt10 d_bl pb8">Место покупки:</span>
                                <textarea class="pragmaticabook font_14 lh_24" name="" id="" cols="30" rows="10">«Хочу попасть в Книгу рекордов Гиннеса как первый человек, пробежавший марафон на 15-сантиметровых шпильках. Голосуйте за меня!»
                                </textarea>
                                <span class="pragmaticabook description_photo_span font_14 lh_18 d_bl gray2 pb12  brdb_dashed2">
                                    * неизменно после проверки модератором
                                </span>
                                <form class="clearfix pt21">
                                    <button type="button" class="bg_green btn5 white font_14 f_left d_ib popup_open" data-modal="#save">Сохранить</button>
                                    <div class="select_file d_ib f_right">
                                        <input type="file" class="d_ib" value="Выбрать файл">
                                        <button type="button" class="btn5 bg_gray3 white d_bl font_14">Изменить фото</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="profile_box brdbmn">
                            <span class="d_bl font_14 lh_24 gray2 pragmaticaLight">Информация об участнике</span>
                                <div class="clearfix br_bot pb10 profile_box_contact">
                                    <div class="f_left profile_e_mail">
                                        <span class="d_bl black">E-mail:</span>
                                        <span class="d_bl pragmaticaBook black font_14">anastasia_voloschkevich1985@gmail.com</span>
                                    </div>
                                    <div class="f_right profile_tel">
                                        <span class="d_bl black">Телефон:</span>
                                        <span class="d_bl pragmaticaBook black font_14">+7 (435) 234-61-17</span>
                                    </div>
                                </div>
                            <div class="profile_checks pb10">
                                <span class="d_bl font_14 lh_24 gray2 pragmaticaLight">Добавьте больше чеков и увеличьте свои шансы на победу.</span>
                               
                                 <form class="information_prof">
                                    <span class="title4 black pt10 d_bl pb8">Место покупки:</span>
                                    <div class="radio_profile">
                                        <div class="profile_radio_box">
                                            <div class="d_ib profile_place">
                                                <input class="d_ib jq-radio styler1 mt-5" type="radio" name="place_item" id="salon3" checked="checked">
                                                <label class="profile_label d_ib pragmaticabook black font_14" for="salon3">Салон ALBA</label>
                                            </div>
                                            <div class="d_ib profile_place">
                                                <input class="d_ib jq-radio styler1 mt-5" type="radio" name="place_item" id="magazine3">
                                                <label class="profile_label pragmaticabook black font_14 d_ib" for="magazine3">Интернет магазин</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="grid_6">
                                                <div class="f_left">
                                                    <select class="styler prof_select d_ib" name="" id="">
                                                        <option value="Салон Бум, Москва, ул. Перерва 43">Салон Бум, Москва, ул. Перерва 43</option>
                                                        <option value="Салон Бум, Москва, ул. Перерва 43">Салон Бум, Москва, ул. Перерва 43</option>
                                                        <option value="Салон Бум, Москва, ул. Перерва 43">Салон Бум, Москва, ул. Перерва 43</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="grid_6">
                                                <div class="number_reg2 p_rel">
                                                    <span class="title4 black d_bl pt13 pb8">Номер 
                                                        <a href="#" class="lk2 checker">чека
                                                            <span class="reg_check22 p_abs"><img src="images/reg_check.png" alt=""></span>
                                                        </a> или <a href="#" class="lk2">заказа</a>:
                                                    </span>
                                                    <input type="text">
                                                    <span class="pragmaticabook font_14 lh_18 d_bl gray2 pt8 pr50">
                                                        * в конкурсе участвуют чеки, в которых присутствует товар из ассортимента обуви и/или сумок : только товар TM ALBA на сумму 7000 рублей и более.
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row prefix_2">
                                            <div class="grid_2 mt16 mb18">
                                                <button type="button" class="btn4 font_14 lh_24 metaM pt8 pb8 popup_open white bg_red pragmaticabook" data-modal="#save_check">Добавить</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                 <div class="table_prof">
                                    <table id="checks">
                                        <tr>
                                           <th class="fw500 black pragmaticaMedium">Место покупки</th>
                                            <th class="fw500 black pragmaticaMedium">Номер чека</th>
                                        </tr>
                                        <tr>
                                            <td>Салон Бум, Москва, ул.Перерва 43</td>
                                            <td class="check_true">1948.18</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
  <!--============================== Footer ==============================-->
    <?php include("main_blocks/footer.php") ?>
</body>
</html>